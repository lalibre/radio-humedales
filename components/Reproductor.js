import Sound from 'react-native-sound';
import React, { useState, useEffect } from 'react';
import { View, Image, TouchableOpacity, StyleSheet, ActivityIndicator } from 'react-native';
import playButton from '../assets/play.png';
import pauseButton from '../assets/pause.png';

export default function Reproductor() {
  const [isPlaying, setIsPlaying] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [sound, setSound] = useState(null);

  useEffect(() => {
    loadSound();
    return () => {
      if (sound) {
        sound.release();
      }
    };
  }, []);

  const loadSound = () => {
    const soundInstance = new Sound('https://radio.lalibre.net/radiohumedales', null, (error) => {
      if (error) {
        console.error('Failed to load sound', error);
        setIsLoading(false);
        return;
      }
      setSound(soundInstance);
      setIsLoading(false);
    });
  };

  const playSound = () => {
    if (sound) {
      sound.play((success) => {
        if (!success) {
          console.error('Sound playback failed');
        }
      });
      setIsPlaying(true);
    }
  };

  const pauseSound = () => {
    if (sound) {
      sound.pause();
      setIsPlaying(false);
    }
  };

  return (
    <View style={styles.container}>
      {isLoading ? (
        <ActivityIndicator size="large" color="#0000ff" />
      ) : (
        <TouchableOpacity onPress={isPlaying ? pauseSound : playSound}>
          <Image style={styles.playPauseButton} source={isPlaying ? pauseButton : playButton} />
        </TouchableOpacity>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  playPauseButton: {
    width: 80,
    height: 80,
    resizeMode: 'contain',
    paddingTop: 40,
  },
});
