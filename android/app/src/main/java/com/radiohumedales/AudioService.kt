package com.radiohumedales

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.util.Log
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.app.NotificationCompat
import com.radiohumedales.R

class AudioService : Service() {
    private val TAG = "AudioService"
    private val CHANNEL_ID = "AudioServiceChannel"
    private var isPlaying = false
    private var webView: WebView? = null

    companion object {
        private var instance: AudioService? = null

        fun getInstance(): AudioService? {
            return instance
        }

        fun isServiceRunning(context: Context): Boolean {
            return instance != null
        }
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        Log.d(TAG, "AudioService created")
        createNotificationChannel()

        webView = WebView(this)
        webView?.settings?.javaScriptEnabled = true
        webView?.webViewClient = WebViewClient()
        webView?.loadUrl("file:///android_asset/reproductor.html")

        // Start foreground service with notification
        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle("Audio Service")
            .setContentText("Playing audio")
            .setSmallIcon(R.drawable.ic_pause)
            .build()

        startForeground(1, notification)
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "AudioService destroyed")
        stopAudio()
        instance = null
    }

    private fun stopAudio() {
        if (isPlaying) {
            // Call JavaScript function to stop audio playback
            webView?.evaluateJavascript("stopAudio();", null)
            isPlaying = false
        }
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(
                CHANNEL_ID,
                "Audio Service Channel",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            val manager = getSystemService(NotificationManager::class.java)
            manager.createNotificationChannel(serviceChannel)
        }
    }
}