package com.radiohumedales

import android.app.Application
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.PowerManager
import android.provider.Settings
import com.facebook.react.PackageList
import com.facebook.react.ReactApplication
import com.facebook.react.ReactHost
import com.facebook.react.ReactNativeHost
import com.facebook.react.ReactPackage
import com.facebook.react.defaults.DefaultNewArchitectureEntryPoint.load
import com.facebook.react.defaults.DefaultReactHost.getDefaultReactHost
import com.facebook.react.defaults.DefaultReactNativeHost
import com.facebook.soloader.SoLoader
import com.radiohumedales.AudioService

class MainApplication : Application(), ReactApplication {

  override val reactNativeHost: ReactNativeHost =
      object : DefaultReactNativeHost(this) {
        override fun getPackages(): List<ReactPackage> =
            PackageList(this).packages.apply {
              // Packages that cannot be autolinked yet can be added manually here, for example:
              // add(MyReactNativePackage())
            }

        override fun getJSMainModuleName(): String = "index"

        override fun getUseDeveloperSupport(): Boolean = BuildConfig.DEBUG

        override val isNewArchEnabled: Boolean = BuildConfig.IS_NEW_ARCHITECTURE_ENABLED
        override val isHermesEnabled: Boolean = BuildConfig.IS_HERMES_ENABLED
      }

  override val reactHost: ReactHost
    get() = getDefaultReactHost(applicationContext, reactNativeHost)

  override fun onCreate() {
    super.onCreate()
    SoLoader.init(this, false)

    // Start the AudioService
    val intent = Intent(this, AudioService::class.java)
    startForegroundService(intent)
    
    // Verificar y solicitar exclusión de la optimización de batería
    // if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
    //     try {
    //         val pm = getSystemService(Context.POWER_SERVICE) as PowerManager
    //         if (!pm.isIgnoringBatteryOptimizations(packageName)) {
    //             val intent = Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS)
    //             intent.data = Uri.parse("package:$packageName")
    //             intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
    //             startActivity(intent)
    //         }
    //     } catch (e: Exception) {
    //         e.printStackTrace()
    //         // Manejar la excepción según sea necesario
    //     }
    // }

    if (BuildConfig.IS_NEW_ARCHITECTURE_ENABLED) {
      // If you opted-in for the New Architecture, we load the native entry point for this app.
      load()
    }
  }
}