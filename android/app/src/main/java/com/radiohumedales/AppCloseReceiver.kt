package com.radiohumedales

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

class AppCloseReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        if (context != null) {
            Log.d("AppCloseReceiver", "Stopping AudioService due to app close")
            context.stopService(Intent(context, AudioService::class.java))
        }
    }
}