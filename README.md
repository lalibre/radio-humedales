# Radio Humedales - React Native App

Esta es la aplicación móvil de Radio Humedales desarrollada con React Native para dispositivos Android.

## Requisitos Previos

- **Node.js** (v14 o superior)
- **npm** o **yarn**
- **JDK 11** o **JDK 17**
- **Android Studio** con SDK de Android configurado

## Instalación y Configuración

1. **Clonar el repositorio**:

   ```bash
   git clone https://github.com/tu-usuario/tu-repositorio.git
   cd tu-repositorio
   ```

2. **Instalar dependencias**:

   ```bash
   npm install
   # o
   yarn install
   ```

3. **Configurar `JAVA_HOME`**:

   Asegúrate de que `JAVA_HOME` esté configurado para JDK 11 o 17:

   - **Windows**: Configura en "Variables de entorno" con la ruta `C:\Program Files\Java\jdk-11.0.12`.
   - **macOS/Linux**: Añade esto a tu archivo de configuración de shell (`.bashrc`, `.zshrc`, etc.):

     ```bash
     export JAVA_HOME=$(/usr/libexec/java_home -v 11)
     export PATH=$JAVA_HOME/bin:$PATH
     ```

## Compilación y Generación de APK

1. **Generar APK de Release**:

   - Sin configuración de firma de producción, usando la firma de depuración:

     ```bash
     cd android
     ./gradlew assembleRelease
     ```

   - El APK se generará en `android/app/build/outputs/apk/release/app-release-unsigned.apk`.

## Ejecución en Modo Debug

1. **Ejecutar la app en modo debug**:

   ```bash
   npx react-native run-android
   ```

   Esto instalará y ejecutará la aplicación en un dispositivo conectado o emulador.

## Distribución

El APK (`app-release-unsigned.apk`) generado se puede distribuir directamente para pruebas internas.

## Licencia

Este proyecto está bajo la Licencia MIT.