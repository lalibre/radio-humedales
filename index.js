/**
 * @format
 */

import { AppRegistry, Image, Text, View, StyleSheet, TouchableOpacity, Linking } from 'react-native';
import { StatusBar } from 'react-native';
import React, { useState } from 'react';
import logo from './assets/logo.png'; // Asegúrate de que la ruta sea correcta
import facebook from './assets/facebook.png';
import twitter from './assets/twitter.png';
import instagram from './assets/instagram.png';
import whatsapp from './assets/whatsapp.png';
import Reproductor from './components/Reproductor';

// Este es el contenido que estaba en App.js
function App() {
  const [isStreamOnline, setIsStreamOnline] = useState(true);

  return (
    <View style={styles.container}>
      <Image source={logo} style={styles.logo}></Image>
      <Text style={styles.texto}>Somos una radio libre, comunitaria, feminista, autónoma, 100% autogestionada y sin fines de lucro.

¡Estamos al aire desde julio de 2017, gracias al trabajo activista de nuestra colectiva radialista y también gracias a tu apoyo!</Text>
      <View style={styles.box}>
        <Text style={styles.textoEscuchanos}>Escúchanos aquí!</Text>
        <Reproductor onStreamStatusChange={setIsStreamOnline} />
        {!isStreamOnline && <Text style={styles.offlineText}>Offline, revisa nuestra programación.</Text>}          
      </View>
      <View style={styles.redes}>
        <Text>Síguenos en:</Text>
        <View style={styles.socialMediaContainer}>
          <TouchableOpacity onPress={() => Linking.openURL('https://www.facebook.com/culturayexistencialesbica/')}>
            <Image source={facebook} style={styles.socialMediaIcon}></Image>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => Linking.openURL('https://twitter.com/radiohumedales')}>
            <Image source={twitter} style={styles.socialMediaIcon}></Image>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => Linking.openURL('https://www.instagram.com/radio_humedales/')}>
            <Image source={instagram} style={styles.socialMediaIcon}></Image>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => Linking.openURL('https://wa.link/9e5a7d')}>
            <Image source={whatsapp} style={styles.socialMediaIcon}></Image>
          </TouchableOpacity>
        </View>
      </View>
      <View>
        <TouchableOpacity onPress={() => Linking.openURL('https://lalibre.net/')}>
          <Text style={styles.footer}>Hecho con ❤ por <Text style={{textDecorationLine: 'underline'}}> LaLibre.net </Text></Text>
        </TouchableOpacity>
      </View>
      <StatusBar style="auto" />
    </View>
  );
}

// Estilos
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#57C2B0',
    justifyContent: 'center',
  },
  logo: {
    width: 190, 
    height: 190, 
    resizeMode: 'contain', 
    alignSelf: 'center',
  },
  box: {
    backgroundColor: '#FAE48E',
    padding: 25, 
    alignItems: 'center',
    width: 300, 
    height: 210,
    borderColor: 'black',
    borderWidth: 3,
    borderRadius: 10, 
    alignSelf: 'center',
  },
  texto: {
    color: 'black',
    textAlign: 'center',
    fontSize: 15,
    padding: 20,
  },
  redes: {
    alignItems: 'center',
    width: 350,
    height: 110,
    paddingTop: 20,
    paddingBottom: 20,
    justifyContent: 'center',
    alignSelf: 'center', 
  },
  socialMediaContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: 180,
    paddingTop: 20,
  },
  socialMediaIcon: {
    width: 35,
    height: 35, 
    resizeMode: 'contain',
  },
  footer: {
    color: 'black',
    textAlign: 'center',
    fontSize: 11,
    padding: 50,
  },
  textoEscuchanos: {
    color: 'black',
    textAlign: 'center',
    fontSize: 18,
    padding: 10,
  },
  offlineText: {
    color: 'red',
    textAlign: 'center',
    fontSize: 14,
    padding: 10,
  },
});

// Registro de la aplicación principal
AppRegistry.registerComponent('RadioHumedales', () => App);

// Necesario si quieres usar React Native en la web también
if (Platform.OS === 'web') {
  const rootTag = document.getElementById('root') || document.getElementById('app');
  AppRegistry.runApplication('RadioHumedales', { initialProps: {}, rootTag });
}

